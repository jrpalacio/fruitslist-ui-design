const app = new Vue({
    el:"#app",
    data: {
        title: "Lista de frutas",
        fruits: [
            {
                name: "Mangos",
                size: 0
            },
            {
                name: "Manzanas",
                size: 0
            }
        ],
        fruitNew: ""
    },
    methods: {
        addFruit () {
            this.fruits.push({
                name: this.fruitNew,
                size: 1
            })
        }
    }
})
